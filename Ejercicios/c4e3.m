% Clase 4 Ejercicio 3
% a. Una v.a. continua X responde a un proceso Gaussiano de media cero y 
%   ?^2 = 1.
% b. Simular varias realizaciones de X y estimar la pdf usando el metodo
%   del histograma.
% c. Variar la cantidad de bins y sacar conclusiones acerca de la calidad de
%   estimacion.
% -----------------------------------------------------------------------------
pkg load statistics;

% Item a
N = 10000;

% Vector de realizaciones de la v.a. X
x = randn(N,1)

% Histogramas con default, 100, 200 y 1000 bins
subplot(2,2,1)
hist(x)
title('hist(x)')

subplot(2,2,2)
hist(x,100)
title('hist(x, 100)')

subplot(2,2,3)
hist(x,200)
title('hist(x, 200)')

subplot(2,2,4)
hist(x,1000)
title('hist(x, 1000)')

% A medida que aumentan los bins tengo mas resolucion, pero llega un punto
%   a partir del cual las ganancias son cada vez menores y empiezo a tener mas
%   ripple en la amplitud de los bins.