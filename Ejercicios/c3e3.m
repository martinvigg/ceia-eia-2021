% Clase 3 - Ejercicio 3
% a. Sean X, Y v.a. U[0, 1] independientes. Definamos Z = X + Y. Encontrar 
%   E[Z|X], E[X|Z], E[XZ|X], E[XZ|Z].
% b. Simular y estimar dichas esperanzas.
% -----------------------------------------------------------------------------

% Item a
% E[Z|X] = E[X+Y|X]
% E[Z|X] = E[X|X] + E[Y|X]
% E[Z|X] = X + E[Y] (por ser X e Y independientes, E[Y|X] = E[Y])
% E[Z|X] = X + 1/2

% E[X|Z] = E[X|X+Y]
% E[X|Z] = 

% E[XZ|X] = E[X(X+Y)|X]
% E[XZ|X] = E[XX+XY|X]
% E[XZ|X] = E[XX|X] + E[XY|X]
% E[XZ|X] = X^2 + E[X|X] * E[Y|X] (por ser X e Y independientes, 
%                                  � E[XY|X] = E[X|X]*E[Y|X] ? )
% E[XZ|X] = X^2 + X * 1/2

% Item b
n = 10000

X = rand(n, 1);
Y = rand(n, 1);

Z = X+Y;

% Como X es discreta, tengo que preguntar por un rango.
valor = .75
delta = 0.01

% Simulo una medicion X = x = valor con tolerancia delta.
X_index = find((X > valor - delta) & (X < valor + delta));
Z_index = find((Z > valor - delta) & (Z < valor + delta));

E_Z_dado_X_Teorico = valor + 0.5
% Calculo la esperanza E[Z|X] como la media de los valores de Z en
%   los indices donde X = x = valor.
E_Z_dado_X = mean(Z(X_index))

E_X_dado_Z_Teorico = valor / 2
% Calculo la esperanza E[X|Z] como la media de los valores de Z en
%   los indices donde Z = z = valor.
E_X_dado_Z = mean(X(Z_index))

E_XZ_dado_X_Teorico = valor * (valor + 0.5)
% Calculo la esperanza E[XZ|X] como la media de los valores de Z en
%   los indices donde X = x = valor multiplicado por la media de X.
E_XZ_dado_X = mean(X(X_index)) * mean(Z(X_index))

E_XZ_dado_Z_Teorico = valor * valor / 2
% Calculo la esperanza E[XZ|Z] como la media de los valores de X en
%   los indices donde Z = z = valor multiplicado por la media de Z.
E_XZ_dado_Z = mean(Z(Z_index)) * mean(X(Z_index))


