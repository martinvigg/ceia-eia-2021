pkg load statistics;

X = [1 2 3 4];
Y = X;
c1 = conv(X,Y);

% Si Y fuera la respuesta de un filtro, c2 seria la salida de X pasado por Y.
% Y corresponde a una media movil.
X = [1 2 3 1 2 3 1 2 3];
Y = 1/3 .* [1 1 1];
c2 = conv(X,Y);

% Z = X + Y, con X, Y = U[0,1].
x1 = ones(1,100)
x2 = ones(1,100)

z = conv(x1, x2),
%stem(z);

% Z = X + Y, con X, Y = N(0,1).
x1 = mvnpdf(linspace(-3, 3, 100).').';
x2 = mvnpdf(linspace(-3, 3, 100).').';

z = conv(x1, x2);
%stem(z)
%hist(z)

% X = aU + bV, Y = cU + dV, con U, V = N(0,1)
n = 1000
U = randn(n, 1);
V = randn(n, 1);

a = 0.5;
b = 0.25;
c = 0.1;
d = 0.7;

X = a*U+b*V;
Y = c*U+d*V;

scatter(X,Y)
corrcoef(X,Y)