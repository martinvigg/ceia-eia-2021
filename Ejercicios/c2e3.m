% Clase 2 - Ejercicio 3
% a. Usando el metodo de la transformada inversa, calcular la pdf de la 
% distribucion exponencial fY (y) = ?e^(??y) para y ? 0, o fY (y) = 0 si
% y < 0. Usar un valor arbitrario de ? > 0.
% b. Simular en Octave.
% -----------------------------------------------------------------------------
pkg load statistics;
