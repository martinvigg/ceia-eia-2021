% Clase 4 Ejercicio 4
% a. Una v.a. continua X responde a un proceso Gaussiano de media cero y 
%   ?^2 = 1.
% b. Simular varias realizaciones de X y estimar la pdf un estimador de kernel 
%   gaussiano.
% c. Variar la el parámetro h y sacar conclusiones acerca de la calidad de
%   estimacion.
% -----------------------------------------------------------------------------
pkg load statistics;
pkg load econometrics;

% Item a
N = 10000;

% Vector de realizaciones de la v.a. X
x = randn(N,1);

% Puntos de evaluacion de la pdf
N_eval = 200;
x_eval = linspace(-3, 3, N_eval);

pdf_teorica = normpdf(x_eval, 0 , 1);
plot(x_eval, pdf_teorica, "displayname", 'Teorica' )
hold on

for bW = [0.01 0.05 0.1 0.125 0.15 1 ]
  
  dens = kernel_density(x_eval.', x, bW);

  plot(x_eval, dens, "displayname", mat2str(bW) )
  legend()  
  
end

% A medida que aumenta el ancho de banda la pdf estimada resulta mas redondeada.
% Si el ancho de banda es muy chico, se genera ripple ya que se 've' el efecto 
%   de cada muestra.
% Si el ancho de banda es muy grande, se suaviza por demas la pdf.