% Clase 1 - Ejercicio 3
% Sean X, Y dos v.a. i.i.d. U[0, 1]. Encontrar la expresion de la funcion de
%   densidad de probabilidad conjunta.
% a. �Cual es la probabilidad de que X > 0.7 y Y < 0.4 simultaneamente?
% b. �Cual es el percentil 40 de X, i.e. x40?
% c. Simular en Octave escribiendo la pdf conjunta o usando funciones de
%   Octave rand,unifpdf,unifcdf.
% d. Graficar la cdf en funcion de x, y.
% -----------------------------------------------------------------------------
pkg load statistics;

% Item a
% Como son independientes, la probabilidad P(X>0.7 ^ Y<0.4) es el producto de
%   las probabilidades de P(X>0.7) y P(Y<0.4).
p_x_may = 1 - unifcdf(0.7)
p_y_men = unifcdf(0.4)
p_x_may_y_men_unifcdf = p_x_may * p_y_men

% Item b
% El percentil 40 de X ser�a el xp tal que P(X<xp) = 0.4.
% En el caso de la U[0,1], corresponde al 0.4.
xp_40 = unifcdf(0.4)

% Item d
% Limites de x e y.
x_a = 0
x_b = 1
y_a = 0
y_b = 1

% Defino x e y como un espacio lineal de 0 a 1, con 100 elementos.
x = linspace(x_a, x_b, 100)
y = linspace(y_a, y_b, 100)

% Cargo en dos vectores la cdf de cada variable.
x_cdf = unifcdf(x, x_a, x_b)
y_cdf = unifcdf(y, y_a, y_b)
x_pdf = unifpdf(x, x_a, x_b)
y_pdf = unifpdf(y, y_a, y_b)

% Armo una matriz con la cdf conjunta en funcion de x e y
for i = 1:length(x)
  for j = 1:length(y)    
    z_cdf(i,j) = x_cdf(i)*y_cdf(j);
    z_pdf(i,j) = x_pdf(i)*y_pdf(j);
  end
end

% Ploteo
%surface(x,y,z_cdf)
mesh(x,y,z_cdf)
title('CDF(x,y)')

figure

%surface(x,y,z_pdf)
mesh(x,y,z_pdf)
title('PDF(x,y)')