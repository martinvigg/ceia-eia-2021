% Clase 3 - Ejercicio 4
% a. Sea un proceso AWGN y(n) = 2 + w(n) donde w(n) = N (0, 1)
% b. Estimar la media y varianza de y(n) usando los siguientes estimadores:
% c. y^ = 1/N sum(0, N - 1, y(i))
% d. s_n = 1/N sum(0, N - 1, (y(i)- y^)^2) 
% e. s_n-1 = 1/(N-1) sum(0, N - 1, (y(i)- y^)^2) 
% f. Calcular la esperanza de cada estimador
% g. Simular con N = 10 y N = 10000
% h. Interpretar los valores de s_n, s_n?1 en cada caso. �Cual es mejor de los
%   dos?
% -----------------------------------------------------------------------------

% Item a
n = 10;
y_10 = 2 * ones(n,1) + randn(n, 1);
n = 10000;
y_10000 = 2 * ones(n,1) + randn(n, 1);

y_e_teorica = 2

% Item b
y_e_10 = 0;
for i = 1:length(y_10)
  y_e_10 += y_10(i);
end

y_e_10 = y_e_10 / length(y_10)

y_e_10000 = 0;
for i = 1:length(y_10000)
  y_e_10000 += y_10000(i);
end

y_e_10000 = y_e_10000 / length(y_10000)

y_var_teorica = 1

% Item c
s_n_10 = 0;
for i = 1:length(y_10)
  s_n_10 += (y_10(i) - y_e_10)^2;
end

s_n_10 = s_n_10 / length(y_10)

s_n_10000 = 0;
for i = 1:length(y_10000)
  s_n_10000 += (y_10000(i) - y_e_10000)^2;
end

s_n_10000 = s_n_10000 / length(y_10000)

% Item d
s_n_1_10 = 0;
for i = 1:length(y_10)
  s_n_1_10 += (y_10(i) - y_e_10)^2;
end

s_n_1_10 = s_n_1_10 / (length(y_10) - 1)

s_n_1_10000 = 0;
for i = 1:length(y_10000)
  s_n_1_10000 += (y_10000(i) - y_e_10000)^2;
end

s_n_1_10000 = s_n_1_10000 / (length(y_10000) - 1)

% Item e
% y^ = 1/N sum(0, N - 1, y(i))
% E[y^] = E[1/N sum(0, N - 1, y(i))]
% E[y^] = 1/N * E[sum(0, N - 1, y(i))]
% E[y^] = 1/N * sum(0, N - 1, E[Y])
% E[y^] = 1/N * sum(0, N - 1, 2) (E[Y] = E[2+W] = E[2] + E[W] = 2 + 0)
% E[y^] = 1/N * 2 * N 
% E[y^] = 2

% s_n = 1/N sum(0, N - 1, (y(i)- y^)^2) 
% E[s_n] = E[1/N sum(0, N - 1, (y(i)- y^)^2)]
% E[s_n] = 1/N * E[sum(0, N - 1, (y(i)- y^)^2)]
% E[s_n] = 1/N * sum(0, N - 1, var[Y])
% E[s_n] = 1/N * sum(0, N - 1, var[2 + W])
% E[s_n] = 1/N * sum(0, N - 1, var[2] + var[W]) (por ser 2 y W independientes)
% E[s_n] = 1/N * sum(0, N - 1, var[W])
% E[s_n] = 1/N * sum(0, N - 1, 1)
% E[s_n] = 1/N * 1 * N
% E[s_n] = 1

% s_n-1 = 1/(N-1) sum(0, N - 1, (y(i)- y^)^2) 
% idem a s_n...
% E[s_n-1] = 1/(N-1) * sum(0, N - 1, 1)
% E[s_n-1] = 1/(N-1) * 1 * N
% E[s_n-1] = N/(N-1) * 1 

% Item h
% Para n chico (n = 10) observo mayor diferencia entre s_n y s_n-1 que para 
% n grande (n = 10000).