% Clase 3 - Ejercicio 2
% a. Sea la longitud de una vara L. Supongamos que optamos por cortar la vara en 
%   un lugar elegido uniformemente al azar Y . Nos quedamos con la parte de 
%   vara de longitud entre [0, Y]. Luego nuevamente decidimos partir la porcion 
%   restante en un lugar aleatoriamente elegido uniformemente, y llamamos a la 
%   longitud resultante X (Pista: usar la ley de esperanzas iteradas).
% b. Encontrar la expresion de E[X] en funcion de L.
% c. Encontrar la expresion de var[X] en funcion de L.
% d. Simular el proceso con N = 1000 ensayos y encontrar la media y
%   varianza muestral de X.
% -----------------------------------------------------------------------------

% Item b
% Y es una v.a. que representa el punto donde se corta la vara.
% X es una v.a. que representa el segundo punto donde se corta la vara.
% 
% La esperanza de X dado Y (E[X|Y]) es igual a Y/2 por ser X uniformemente 
%   distribuida.
% La esperanza de Y (E[Y]) es igual a L/2 por ser Y uniformemente distribuida.
% Luego, aplicando la ley de esperanzas iteradas:
% E[X] = E[E[X|Y]]
% Pero E[X|Y] es igual a la v.a. Y/2, luego:
% E[X] = E[Y/2]
% E[X] = 1/2 * E[Y]
% E[X] = 1/2 * L/2
% E[X] = L/4

% Item c
% Y es una v.a. que representa el punto donde se corta la vara.
% X es una v.a. que representa el segundo punto donde se corta la vara.
% 
% Luego, aplicando la ley de varianzas condicionadas:
% var[X] = E[var(X|Y)] + var(E[X|Y])
%
% Recordando que E[X|Y] es igual a la v.a. Y/2:
% var(E[X|Y]) = var(Y/2)
% var(E[X|Y]) = 1/4 * var(Y)
% var(E[X|Y]) = 1/4 * L^2/12
% var(E[X|Y]) = L^2/48
%
% Como X esta uniformemente distribuida entre 0 e Y, la varianza de X dado Y
%   es:
% var(X|Y) = Y^2/12
%
% Luego:
% E[var(X|Y)] = E[Y^2/12]
% E[var(X|Y)] = 1/12 * E[Y^2]
% E[var(X|Y)] = 1/12 * E[Y^2]
%
% Calculamos E[Y^2]:
% E[Y^2] = int(0, L, y^2 * 1/L)
% E[Y^2] = y^3/(3L) | 0, L
% E[Y^2] = L^3/(3L)
% E[Y^2] = L^2/3
%
% Finalmente:
% E[var(X|Y)] = 1/12 * L^2/3
% E[var(X|Y)] = L^2/36
%
% Podemos calcular la var(X) como:
% var[X] = E[var(X|Y)] + var(E[X|Y])
% var[X] = L^2/36 + L^2/48
% var[X] = L^2 * 84/1728

% Item d
n = 1000
L = 1

X = zeros(n, 1);
Y = rand(n, 1) * L;

for i = 1:length(Y)
  X(i) = rand() * Y(i);
end

mediaX = 0;

for i = 1:length(X)
  mediaX += X(i);
end

mediaX /= n
mediaTeorica = L/4

varianzaX = 0;

for i = 1:length(X)
  varianzaX += (X(i) - mediaX)^2;
end

varianzaX /= n
varianzaTeorica = L^2 * 84/1728

