% Clase 4 - Ejercicio 2
% a. Se tira una moneda 100 veces, y salen 55 cecas.
% b. Encontrar el estimador de maxima verosimilitud de la probabilidad de
%   ceca p.
% c. Simular el experimento y encontrar por computadora el valor de la
%   estimacion de maxima verosimilitud de p.
% -----------------------------------------------------------------------------
pkg load statistics;

% Item c
N = 10000;
k = 100;
x = 55;

% Vector de verosimilitud
P = zeros(N,1);

% Vector de probabilidades ([0, 1] en N puntos)
p = linspace(0, 1, N);

% Calculo la pdf para cada P
for i = 1:N
  P(i) = binopdf(x, k, p(i));
end

% Busco el valor maximo de P
[P_max, P_max_index] = max(P);

% Busco el p que hace maximo a P
p_max_verosimilitud = p(P_max_index)
