% Clase 1 - Ejercicio 1
% a. �Cual es la probabilidad de obtener exactamente 3 cecas en 10 tiradas de 
%   una moneda balanceada?
% b. �Cual es la probabilidad de obtener al menos 3 cecas en 10 tiradas de una
%   moneda cargada donde la probabilidad de ceca es 0.4?
% c. Una moneda cargada con probabilidad de ceca 0.4 es arrojada al aire. 
%   El resultado es cara. �Cual es la probabilidad de obtener al menos 3 cecas
%   en las proximas 10 tiradas?
% d. Simular los items anteriores en Octave usando: 
%   i. Una distribucion uniforme con la funcion rand para simular el proceso
%     Bernoulli.
%   ii. Las funciones binopdf,binocdf para calcular la probabilidad binomial
%     (Octave statistics package).
% -----------------------------------------------------------------------------
pkg load statistics;

% Item a

p = 0.5 % Probabilidad de obtener ceca
k = 3   % Cantidad de cecas
n = 10  % Cantidad de tiradas

% Probabilidad de obtener exactamente 3 cecas en 10 tiradas de una moneda 
%   balanceada usando la funcion de probabilidad.
Prob3CecasEn10 = factorial(n)/(factorial(k) * factorial(n-k)) * p^k * (1-p)^(n-k)

% Probabilidad de obtener exactamente 3 cecas en 10 tiradas de una moneda 
%   balanceada usando la funcion de probabilidad binopdf.
Prob3CecasEn10_binopdf = binopdf(k, n, p)

% Item b

p = 0.4 % Probabilidad de obtener ceca
k = 3   % Cantidad de cecas
n = 10  % Cantidad de tiradas

% Probabilidad de obtener al menos 3 cecas en 10 tiradas de una moneda 
%   balanceada usando la funcion de probabilidad.
% Se hace la sumatoria de P(X = k) para k = 0, 1, 2 y luego se resta el valor
%   obtenido de 1 para obtener P(X>3).

acum_p_x = 0

for i = 0:2
  p_k_en_n = factorial(n)/(factorial(i) * factorial(n-i)) * p^i * (1-p)^(n-i)
  acum_p_x += p_k_en_n
end

ProbAlMenos3En10 = 1 - acum_p_x

% Probabilidad de obtener al menos 3 cecas en 10 tiradas de una moneda 
%   balanceada usando la funcion de probabilidad binocdf.
% Se calcula P(X<=2) y luego se lo resta de 1 para obtener P(X>3).
ProbAlMenos3En10_binocdf = 1 - binocdf(k - 1, n, p)

% Item c

% Los sucesos serian independientes, por lo que la probabilidad no cambia.
