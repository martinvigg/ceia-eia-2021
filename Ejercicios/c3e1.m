% Clase 3 - Ejercicio 1
% Juan y Pedro juegan un juego de dados en el cual el que tira el dado
%   mas alto gana. Si ambos tiran el mismo numero, tiran de nuevo hasta que uno
%   gane. 
% a. Juan gano. Encontrar la probabilidad de que haya ganado con un 5. (Pista: 
%   listar todos los pares de tiradas de Juan y Pedro en las que Juan gana, y 
%   encontrar en cuales gana con un 5)
% b. Simular un dado y encontrar una estimacion de la probabilidad anterior.
% -----------------------------------------------------------------------------

% Item a
% Pueden darse las siguientes situaciones en el juego:
% Si Juan saca 1: no puede ganar.
% Si Juan saca 2: gana solo si Pedro saca 1.
% Si Juan saca 3: gana si Pedro saca 2 o 1.
% Si Juan saca 4: gana si Pedro saca 3, 2 o 1.
% Si Juan saca 5: gana si Pedro saca 4, 3, 2 o 1.
% Si Juan saca 6: gana si Pedro saca 5, 4, 3, 2 o 1.
% Es decir, que hay 1 + 2 + 3 + 4 + 5 = 15 casos en los cuales juan puede ganar 
%   y 4 casos en los cuales Juan gana con un 5.
% Luego, si definimos el suceso A como 'Juan gano' y a X como la v.a. que modela
%   el numero de dado que saco Juan, la probabilidad de que Juan haya gando con
%   un 5 dado que Juan gano es:
% P(X=5|A) = casos donde Juan gana con un 5 / casos donde Juan gana
%          = 4/15 ~ 0.266

% Item b
% n = cantidad de tiradas de dados.
n = 100;

dadoJuan = randi([1 6], n, 1);
dadoPedro = randi([1 6], n, 1);

ganaPedro = 0;
ganaJuan = 0;
ganaJuanCon5 = 0;

for i = 1:length(dadoJuan)
  if (dadoJuan(i) > dadoPedro(i))
    ganaJuan++;
    if (5 == dadoJuan(i))
      ganaJuanCon5++;
    endif
  elseif (dadoJuan(i) < dadoPedro(i))
    ganaPedro++;
  endif
end

ganaJuan
ganaJuanCon5
probGanaJuanCon5 = ganaJuanCon5/ganaJuan

